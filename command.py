import subprocess
import typing


class Shell:
    def run(self, command: typing.List[str]) -> subprocess.CompletedProcess:
        raise NotImplementedError()

    def run_pipe(self, command, *, stdin: bool, stdout: bool) -> subprocess.Popen:
        raise NotImplementedError()

    @staticmethod
    def needs_sudo(command):
        return command[0] in ['zfs', 'zpool']


class LocalShell(Shell):
    def run(self, command: typing.List[str]) -> subprocess.CompletedProcess:

        command_list = ['sudo'] if self.needs_sudo(command) else []
        command_list += command

        print('running', command_list)
        return subprocess.run(command_list,
                              stdout=subprocess.PIPE,
                              check=True,
                              universal_newlines=True)

    def run_pipe(self, command, *, stdin: bool, stdout: bool):
        command_list = ['sudo'] if self.needs_sudo(command) else []
        command_list += command

        print('running', command_list)

        return subprocess.Popen(command_list,
                                stdin=subprocess.PIPE if stdin else None,
                                stdout=subprocess.PIPE if stdout else None)


local = LocalShell()


class RemoteShell(Shell):
    def __init__(self, host_str: str, ssh_args: typing.List[str] = None):
        ssh_args = ssh_args or []

        self.host_str = host_str
        self.ssh_args = ssh_args

    def run(self, command: typing.List[str]) -> subprocess.CompletedProcess:

        command_list = ['sudo'] if self.needs_sudo(command) else []
        command_list += command

        command_list = ['ssh'] + self.ssh_args + [self.host_str] + command_list

        return local.run(command_list)

    def run_pipe(self, command, *, stdin: bool, stdout: bool):
        command_list = ['sudo'] if self.needs_sudo(command) else []
        command_list += command

        command_list = ['ssh'] + self.ssh_args + [self.host_str] + command_list

        return local.run_pipe(command_list, stdin=stdin, stdout=stdout)
