#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

backed_up_timestamp_file=$DIR/backed_up_timestamp
days=30


stamp_str=`/sbin/zfs list -t snapshot -o name files_zfs/archive | grep "@zfs-auto-snap_monthly" | tail -n 1 | sed 's/.*@zfs-auto-snap_monthly-//' | sed 's/-/ /g' | awk '{ print $1"-"$2"-"$3;}'`
last_monthly_snap_timestamp=`date -d  $stamp_str +%s`
last_backup_timestamp=`date -r $backed_up_timestamp_file +%s`


backup_needed="false"
if [ $last_backup_timestamp -le $last_monthly_snap_timestamp ]; then
    backup_needed="true"
fi

if [ $backup_needed = "true" ]; then
    echo Backup needed
fi
