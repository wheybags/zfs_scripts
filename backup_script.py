#!/usr/bin/env python3

import zfs
import misc
import command
import arrow
import os
import sys

backup_source_host = command.local
backup_source_pool = zfs.get_pools(backup_source_host)['files_zfs']
backup_source_datasets = list(backup_source_pool.get_datasets().keys())

exclude_datasets = set('space')

backup_dest_host = command.local
if 'usb_zfs_backup' not in zfs.get_pools(backup_dest_host):
    backup_dest_host = command.RemoteShell('office')

try:
    if 'usb_zfs_backup' not in zfs.get_pools(backup_dest_host):
        backup_dest_host = None
except:
    backup_dest_host = None

backed_up_timestamp_file = os.path.dirname(os.path.realpath(__file__)) + "/backed_up_timestamp"

def sync_to_backup():
    backup_pool = zfs.get_pools(backup_dest_host)['usb_zfs_backup']

    for dataset_name in backup_source_datasets:
        dataset = backup_source_pool.get_datasets()[dataset_name]

        do_snap = dataset.get_attribute('com.sun:auto-snapshot') != 'false'

        if not do_snap:
            continue

        transitions = zfs.get_transitions_needed_to_sync(dataset, backup_pool)

        def filter_snaps(snap_name):
            return not snap_name.startswith("zfs-auto-snap_frequent") and not snap_name.startswith("zfs-auto-snap_hourly")

        transitions = list(filter(filter_snaps, transitions))

        def is_acceptable_end(snap_name):
            return snap_name.startswith("zfs-auto-snap_monthly") or not snap_name.startswith("zfs-auto-snap_")

        while len(transitions) and not is_acceptable_end(transitions[-1]):
            transitions = transitions[:-1]

        print(transitions)
        #continue

        transitions = zfs.send_snapshots(dataset, backup_pool, transitions)

        sent_to_dataset = backup_pool.get_datasets()[dataset_name]
        sent_to_dataset.set_attribute('readonly', True)

def delete_old_snapshots(*, dataset: zfs.Dataset, keep: int):
    snapshots = dataset.get_snapshots()
    snapshots = [snapshots[x] for x in snapshots]

    snap_count = len(snapshots)

    if snap_count <= keep:
        return

    for i in range(snap_count - keep):
        snapshots[i].destroy()


def snap_send_and_delete_old(src: zfs.Dataset, dest: zfs.Dataset):
    if src.has_been_modified():
        src.create_snapshot_with_date_time()

    zfs.send_snapshot(src, dest)
    delete_old_snapshots(dataset=src, keep=2)


def snapshot_all():
    for dataset_name in backup_source_datasets:
        dataset = zfs.Dataset(path='files_zfs/' + dataset_name)
        if dataset.has_been_modified():
            dataset.create_snapshot_with_date_time()


def main():
    if len(sys.argv) != 2:
        print('Usage ' + sys.argv[0] + ' cron/backup')
        exit(1)

    with misc.lock():
        #if sys.argv[1] == 'cron':
        #    snapshot_all()
        if sys.argv[1] == 'backup':
            sync_to_backup()
            with open(backed_up_timestamp_file, 'wb') as f:
                pass
main()
