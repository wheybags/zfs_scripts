#!/bin/bash

zfs list -p -t filesystem | grep '^files_zfs/' | awk '{print $1}' | xargs -n 1 zfs-auto-snapshot --debug --verbose $@
