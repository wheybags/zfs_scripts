import command
import typing
import collections
import select
import datetime
import arrow

try:
    import tqdm
except ImportError:
    tqdm = None


class ZfsObject:
    def get_path(self) -> str:
        raise NotImplementedError()

    def get_host(self) -> command.Shell:
        raise NotImplementedError()

    def get_attribute(self, name: str) -> typing.Union[bool, int, float, str, None]:
        output = self.get_host().run(['zfs', 'get', name, self.get_path(), '-p']).stdout.splitlines()

        if len(output) == 1:
            return None

        result = output[1].split()[2]

        try:
            result = int(result)
        except ValueError:
            try:
                result = float(result)
            except ValueError:
                pass

        if result == 'on':
            result = True
        elif result == 'off':
            result = False

        return result

    def set_attribute(self, name: str, value: typing.Union[bool, int, float, str]) -> None:
        if isinstance(value, bool):
            value = 'on' if value else 'off'

        self.get_host().run(['zfs', 'set', '{}={}'.format(name, value), self.get_path()])

    def get_creation_time(self):
        timestamp = self.get_attribute('creation') 
        creation_time = arrow.arrow.Arrow.fromtimestamp(timestamp)
        return creation_time


class Snapshot(ZfsObject):
    def __init__(self, *, dataset: "Dataset" = None, name: str = None, path: str = None, host: command.Shell = None):
        assert (path is None and host is None and dataset is not None and name is not None) or \
               (path is not None and dataset is None and name is None)

        if path is None:
            self.dataset = dataset
            self.name = name
        else:
            dataset_path, snapshot_name = path.rsplit('@', 1)
            host = host or command.local

            self.dataset = Dataset(path=dataset_path, host=host)
            self.name = snapshot_name

            assert self.name in self.dataset.get_snapshots()

    def get_path(self) -> str:
        return '{}@{}'.format(self.dataset.get_path(), self.name)

    def get_host(self) -> command.Shell:
        return self.dataset.get_host()

    def destroy(self):
        print('destroying', self)
        self.get_host().run(['zfs', 'destroy', self.get_path()])

    def __str__(self):
        return "Snapshot({})".format(self.get_path())

    def __repr__(self):
        return self.__str__()


class Dataset(ZfsObject):
    def __init__(self, *, pool: "Pool" = None, name: str = None, path: str = None, host: command.Shell = None):
        assert (path is None and host is None and pool is not None and name is not None) or \
               (path is not None and pool is None and name is None)

        if path is None:
            self.pool = pool
            self.name = name
        else:
            host = host or command.local
            pool_name, dataset_name = path.split('/', 1)
            self.pool = Pool(path=pool_name, host=host)
            self.name = dataset_name

            assert self.name in self.pool.get_datasets()

    def get_path(self) -> str:
        return '{}/{}'.format(self.pool.get_path(), self.name)

    def get_host(self) -> command.Shell:
        return self.pool.get_host()

    def get_snapshots(self) -> "collections.OrderedDict[str, Snapshot]":
        output = self.get_host().run(['zfs', 'list', '-t', 'snapshot', '-o', 'name']).stdout


        snapshot_names = output.splitlines()[1:]

        prefix = self.get_path() + "@"
        snapshot_names = list(filter(lambda x: x.startswith(prefix), snapshot_names))
        snapshot_names = [x[len(prefix):] for x in snapshot_names]

        return collections.OrderedDict((x, Snapshot(dataset=self, name=x)) for x in snapshot_names)

    def create_snapshot(self, name) -> Snapshot:
        self.get_host().run(['zfs', 'snapshot', '{}@{}'.format(self.get_path(), name)])
        return Snapshot(dataset=self, name=name)
    
    def create_snapshot_with_date_time(self) -> Snapshot:
        dt_str = datetime.datetime.now(datetime.timezone.utc).astimezone().isoformat().replace('+', '_')
        new_snapshot_name = "py_zfs_" + dt_str
        return self.create_snapshot(new_snapshot_name)

    def purge_empty_snaps(self, *,  keep_last: int, keep: typing.Set[str] = None) -> None:
        """ Destroys snapshots which are the exact same as the snaps preceding them. Doesn't touch the most recent
            n snaps, indicated by the keep_last parameter"""

        keep = keep or {}

        # turn OrderedDict into a simple list
        snapshots = self.get_snapshots()
        snapshots = [snapshots[x] for x in snapshots]

        # ignore the last "keep" snapshots
        if keep_last is not 0:
            snapshots = snapshots[:-keep_last]

        # prefer to keep the first in a sequence of identical snapshots, not the last
        snapshots = list(reversed(snapshots))

        for i in range(1, len(snapshots)):
            first = snapshots[i]
            second = snapshots[i-1]

            if second.name in keep:
                continue

            same = True
            
            # Figure out if anything changed between these two snapshots.
            #
            # Online sources will say you can use the "used" attribute to detect this, but you can't.
            # "used" seems to display the space you would regain if you deleted the snapshot. So, eg if you make a
            # dataset with a file, then snapshot it once, that snapshot's used attr will report 0, since even if you
            # deleted the snapshot, the file is still there in the _current_ version of the dataset.
            #
            # Therefore, the only _valid_ way of doing this is to use the zfs diff command, but this is pretty slow,
            # because it has to produce an actual list of the differences, and we only care if that list is empty or
            # not. As an optimisation, we check the refer attribute - if two snapshots have different refer values, then
            # they definitely are not the same. Of course, it is theoretically possible for two snapshots to have the
            # same refer but different contents, so in this case we make sure by actually running zfs diff.
            if first.get_attribute('refer') != second.get_attribute('refer'):
                same = False
            else:
                output = first.get_host().run(['zfs', 'diff', first.get_path(), second.get_path()]).stdout.strip()
                if len(output):
                    same = False

            if same:
                second.destroy()

    def has_been_modified(self) -> bool:
        return self.get_attribute('written') > 0

    def __str__(self):
        return "Dataset({})".format(self.get_path())

    def __repr__(self):
        return self.__str__()


class Pool(ZfsObject):
    def __init__(self, *, path: str, host: command.Shell = None, _intern=False):
        self.name = path
        self.host = host or command.local

        if not _intern:
            assert self.name in get_pools(host)

    def get_path(self) -> str:
        return self.name

    def get_host(self) -> command.Shell:
        return self.host

    def create_dataset(self, name: str) -> Dataset:
        self.get_host().run(['zfs', 'create', self.name + "/" + name])
        return self.get_datasets()[name]

    def get_datasets(self) -> typing.Dict[str, Dataset]:
        output = self.get_host().run(['zfs', 'list', '-t', 'filesystem', '-o', 'name']).stdout


        dataset_names = output.splitlines()[1:]

        prefix = self.name + "/"
        dataset_names = list(filter(lambda x: x.startswith(prefix), dataset_names))
        dataset_names = [x[len(prefix):] for x in dataset_names]

        return {x: Dataset(pool=self, name=x) for x in dataset_names}

    def destroy(self):
        print('destroying', self)
        self.get_host().run(['zpool', 'destroy', self.name])

    def __str__(self):
        return "Pool({})".format(self.name)

    def __repr__(self):
        return self.__str__()


def get_pools(host: command.Shell = None) -> typing.Dict[str, Pool]:
    host = host or command.local

    output = host.run(['zpool', 'list', '-o', 'name']).stdout

    pool_names = output.splitlines()[1:]
    return {x: Pool(path=x, host=host, _intern=True) for x in pool_names}


def get_transitions_needed_to_sync(source: Dataset, dest: typing.Union[Dataset, Pool]):
    src_snaps = list(source.get_snapshots().values())

    # If the dest is a pool that contains a dataset of the same name as the source, use that dataset as the dest
    if isinstance(dest, Pool):
        datasets = dest.get_datasets()
        if source.name in datasets:
            dest = datasets[source.name]

    dst_snaps = [] if isinstance(dest, Pool) else list(dest.get_snapshots().values())

    src_i = 0
    dst_i = 0

    most_recent_on_dest = None

    while True:
        if src_i >= len(src_snaps) or dst_i >= len(dst_snaps):
            break

        if src_snaps[src_i].name == dst_snaps[dst_i].name:
            most_recent_on_dest = src_snaps[src_i].name
            src_i += 1

        dst_i += 1

    src_i -= 1


    send_pairs = []
    for to_send_index in range(src_i+1, len(src_snaps)):
        next_snap = src_snaps[to_send_index].name
        send_pairs.append(next_snap)

    return send_pairs
        
def send_snapshots(source: Dataset, dest: typing.Union[Dataset, Pool], transitions):
    do_force = False

    # If the dest is a pool that contains a dataset of the same name as the source, use that dataset as the dest
    if isinstance(dest, Pool):
        datasets = dest.get_datasets()
        if source.name in datasets:
            dest = datasets[source.name]
        #else:
        #    dest = dest.create_dataset(source.name)
        #    do_force = True


    if isinstance(dest, Dataset):
        dest_snap_names = list(dest.get_snapshots().keys()) 
    else:
        dest_snap_names = []

    most_recent_on_dest = None
    if len(dest_snap_names):
        most_recent_on_dest = dest_snap_names[-1]

    for to_send in transitions:
        final_path = "{}/{}@{}".format(source.pool.name, source.name, to_send)

        do_incremental = most_recent_on_dest is not None

        if do_incremental:
            estimate_size_command = ['zfs', 'send', '-n', '-v', '-i', most_recent_on_dest, final_path]
        else:
            estimate_size_command = ['zfs', 'send', '-n', '-v', final_path]

        output = source.get_host().run(estimate_size_command).stdout.splitlines()[-1]

        prefix = "total estimated size is "
        assert output.startswith(prefix)

        unit = output[-1].upper()
        size = round(float(output[len(prefix):-1]))

        if unit == 'G':
            size *= 1024 * 1024 * 1024
        elif unit == 'M':
            size *= 1024 * 1024
        elif unit == 'K':
            size *= 1024
        elif unit == 'B':
            pass
        else:
            print('unknown size in "{}"'.format(output))
            size = 0

        if isinstance(dest, Pool):
            target_dataset = dest.name + "/" + source.name
        else:
            target_dataset = dest.pool.name + "/" + dest.name

        receive_command = ['zfs', 'recv', target_dataset]

        #if target_dataset == 'usb_zfs_backup/host_backups':
        #    receive_command = ['zfs', 'recv', '-F', target_dataset]


        if do_force:
            receive_command += ['-F']
            do_force = False

        if do_incremental:
            send_command = ['zfs', 'send', '-i', most_recent_on_dest, final_path]
        else:
            send_command = ['zfs', 'send', final_path]

        def get_human_data_amount(byte_count):
            if byte_count > 1024 * 1024 * 1024:
                return '{:.2f}GB'.format(byte_count / 1024 / 1024 / 1024)
            if byte_count > 1024 * 1024:
                return '{:.2f}MB'.format(byte_count / 1024 / 1024)
            if byte_count > 1024:
                return '{:.2f}KB'.format(byte_count / 1024)

            return '{}B'.format(byte_count)


        if most_recent_on_dest:
            dest.get_host().run(['zfs', 'rollback', target_dataset + "@" + most_recent_on_dest]).stdout

        send_process = source.get_host().run_pipe(send_command, stdin=False, stdout=True)
        receive_process = dest.get_host().run_pipe(receive_command, stdin=True, stdout=False)

        read_finished = False
        data = None
        sent_bytes = 0

        print('starting send of {}'.format(get_human_data_amount(size)))
        
        if tqdm:
            progress = tqdm.tqdm(total=size, unit='B', unit_scale=True, unit_divisor=1024)
            progress.__enter__()

        while True:
            send_process.poll()
            receive_process.poll()

            if data or read_finished:
                ready = select.select([], [receive_process.stdin], [], 1)

                if receive_process.stdin in ready[1]:
                    receive_process.stdin.write(data)
                    sent_bytes += len(data)

                    if tqdm:
                        progress.update(len(data))
                    else:
                        print('sent: {}%, {}/{}'.format(round(sent_bytes / size * 100),
                                                        get_human_data_amount(sent_bytes),
                                                        get_human_data_amount(size)))
                    data = None

                    if read_finished:
                        break
            else:
                if tqdm:
                    progress.update(0)

                ready = select.select([send_process.stdout], [], [], 1)

                if send_process.stdout in ready[0]:
                    data = send_process.stdout.read(1024 * 1024 * 15)

                    if len(data) == 0:
                        read_finished = True

        assert send_process.poll() == 0
        receive_process.stdin.close()
        receive_process.wait()
        assert receive_process.poll() == 0
        
        if tqdm:
            progress.__exit__()

        print('DONE, sent {}'.format(get_human_data_amount(sent_bytes)))

        most_recent_on_dest = to_send

        if isinstance(dest, Pool):
            datasets = dest.get_datasets()
            dest = datasets[source.name]

        dest.set_attribute('readonly', True)

