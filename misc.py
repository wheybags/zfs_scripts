import fcntl
import errno
import sys
import time

def lock(wait=False):
    f = open('/tmp/wheybags_zfs_cron_lock', 'wb')
   
    def try_lock(): 
        got_lock = False
        try:
            fcntl.flock(f, fcntl.LOCK_EX | fcntl.LOCK_NB)
            got_lock = True
        except IOError as e:
            if e.errno != errno.EAGAIN:
                raise

        return got_lock

    if not try_lock():
        if wait:
            while not try_lock():
                time.sleep(5)
        else:
            print('failed to acquire lock')
            sys.exit(0)

    return f
