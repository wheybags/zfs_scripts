#!/usr/bin/env python3

import command
import zfs
import os
import typing

local = command.LocalShell()

base_dir = os.path.abspath(os.path.dirname(__file__))


def make_test_pool(name: str) -> zfs.Pool:
    image_path = "/tmp/py_zfs_test_" + name + '.img'
    local.run(['truncate', '--size', '1G', image_path])
    local.run(['zpool', 'create', name, image_path])

    return zfs.get_pools(local)[name]


def make_test_pools() -> typing.Tuple[zfs.Pool, zfs.Pool]:
    return make_test_pool("py_zfs_test_1"), make_test_pool("py_zfs_test_2")


def cleanup():
    pools = zfs.get_pools(local)

    for name in ("py_zfs_test_1", "py_zfs_test_2"):
        if name in pools:
            pools[name].destroy()

        image_path = "/tmp/py_zfs_test_" + name + '.img'
        if os.path.exists(image_path):
            os.unlink(image_path)


def make_pools_decorator(test_func):
    def inner():
        test_pools = make_test_pools()
        test_func(test_pools)
        cleanup()

    return inner


@make_pools_decorator
def test_copy_dataset(test_pools: typing.Tuple[zfs.Pool, zfs.Pool]):
    test_dataset = test_pools[0].create_dataset("test_dataset")
    assert "test_dataset" in test_pools[0].get_datasets()
    assert os.path.exists("/py_zfs_test_1/test_dataset")

    local.run(['sudo', 'chown', '-R', '{}:{}'.format(os.getuid(), os.getgid()), '/py_zfs_test_1/test_dataset'])

    with open("/py_zfs_test_1/test_dataset/test.txt", "w") as f:
        f.write("asd")

    # send over an initial snapshot which should create the dataset on the receiving end
    test_dataset.create_snapshot("snap")
    zfs.send_snapshot(test_dataset, test_pools[1])

    assert "test_dataset" in test_pools[1].get_datasets()
    assert os.path.exists("/py_zfs_test_2/test_dataset/test.txt")

    test_dataset.create_snapshot("snap2")
    test_dataset.create_snapshot("snap3")
    test_dataset.create_snapshot("snap4")
    zfs.send_snapshot(test_dataset, test_pools[1])

    dest_snapshots = test_pools[1].get_datasets()["test_dataset"].get_snapshots()

    assert "snap" in dest_snapshots
    assert "snap2" in dest_snapshots
    assert "snap3" in dest_snapshots
    assert "snap4" in dest_snapshots


def test_local_ssh():
    shell = command.RemoteShell('localhost')
    shell.run(['echo', 'a'])


@make_pools_decorator
def test_copy_dataset_over_ssh(test_pools: typing.Tuple[zfs.Pool, zfs.Pool]):

    remote_shell = command.RemoteShell('localhost')
    remote_pools = zfs.get_pools(remote_shell)

    assert test_pools[0].name in remote_pools
    assert test_pools[1].name in remote_pools

    remote_pool = zfs.Pool(path=test_pools[1].name, host=remote_shell)

    test_pools = (test_pools[0], remote_pool)

    test_dataset = test_pools[0].create_dataset("test_dataset")
    assert "test_dataset" in test_pools[0].get_datasets()
    assert os.path.exists("/py_zfs_test_1/test_dataset")

    local.run(['sudo', 'chown', '-R', '{}:{}'.format(os.getuid(), os.getgid()), '/py_zfs_test_1/test_dataset'])

    with open("/py_zfs_test_1/test_dataset/test.txt", "w") as f:
        f.write("asd")

    # send over an initial snapshot which should create the dataset on the receiving end
    test_dataset.create_snapshot("snap")
    zfs.send_snapshot(test_dataset, test_pools[1])

    assert "test_dataset" in test_pools[1].get_datasets()
    assert os.path.exists("/py_zfs_test_2/test_dataset/test.txt")


@make_pools_decorator
def test_attributes(test_pools: typing.Tuple[zfs.Pool, zfs.Pool]):
    pool = test_pools[0]

    pool.set_attribute('readonly', False)
    assert pool.get_attribute('readonly') is False

    pool.set_attribute('readonly', True)
    assert pool.get_attribute('readonly') is True

    error = None
    try:
        open('/' + pool.get_path() + '/newfile', 'w')
    except OSError as e:
        error = e

    assert error
    assert error.errno == 30  # read only filesystem


@make_pools_decorator
def test_purge_empty(test_pools: typing.Tuple[zfs.Pool, zfs.Pool]):
    pool = test_pools[0]

    dataset = pool.create_dataset('test')
    local.run(['sudo', 'chown', '-R', '{}:{}'.format(os.getuid(), os.getgid()), '/' + dataset.get_path()])

    with open('/' + dataset.get_path() + '/newfile', 'w') as f:
        f.write('asd')

    dataset.create_snapshot('snap_1')
    dataset.create_snapshot('snap_2')
    dataset.create_snapshot('snap_3')

    with open('/' + dataset.get_path() + '/newfile2', 'w') as f:
        f.write('asd')

    dataset.create_snapshot('snap_4')
    dataset.create_snapshot('snap_5')
    dataset.create_snapshot('snap_6')
    dataset.create_snapshot('snap_7')
    dataset.create_snapshot('snap_8')
    dataset.create_snapshot('snap_9')

    dataset.purge_empty_snaps(keep_last=2, keep={'snap_6'})
    kept = list(dataset.get_snapshots().keys())
    assert kept == ['snap_1', 'snap_4', 'snap_6', 'snap_8', 'snap_9']

    dataset.purge_empty_snaps(keep_last=0)
    kept = list(dataset.get_snapshots().keys())
    assert kept == ['snap_1', 'snap_4']


@make_pools_decorator
def test_send_with_deleted(test_pools: typing.Tuple[zfs.Pool, zfs.Pool]):
    test_dataset = test_pools[0].create_dataset("test_dataset")

    local.run(['sudo', 'chown', '-R', '{}:{}'.format(os.getuid(), os.getgid()), '/py_zfs_test_1/test_dataset'])

    with open("/py_zfs_test_1/test_dataset/test.txt", "w") as f:
        f.write("asd")

    test_dataset.create_snapshot("snap_1")
    test_dataset.create_snapshot("snap_2")
    test_dataset.create_snapshot("snap_3")
    test_dataset.create_snapshot("snap_4")

    zfs.send_snapshot(test_dataset, test_pools[1])

    target_dataset = test_pools[1].get_datasets()[test_dataset.name]

    assert list(target_dataset.get_snapshots().keys()) == ["snap_1", "snap_2", "snap_3", "snap_4"]

    test_dataset.create_snapshot("snap_5")

    test_dataset.get_snapshots()["snap_2"].destroy()
    test_dataset.get_snapshots()["snap_3"].destroy()

    zfs.send_snapshot(test_dataset, test_pools[1])

    assert list(test_dataset.get_snapshots().keys()) == ["snap_1", "snap_4", "snap_5"]
    assert list(target_dataset.get_snapshots().keys()) == ["snap_1", "snap_2", "snap_3", "snap_4", "snap_5"]


def run_tests():
    cleanup()

    test_send_with_deleted()
    test_purge_empty()
    test_copy_dataset()
    test_local_ssh()
    test_copy_dataset_over_ssh()
    test_attributes()


run_tests()
